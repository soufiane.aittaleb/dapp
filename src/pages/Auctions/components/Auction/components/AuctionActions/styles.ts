import styled from 'styled-components';

export const AuctionActionsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  .auction-button {
    margin-right: 10px;
  }
`;
