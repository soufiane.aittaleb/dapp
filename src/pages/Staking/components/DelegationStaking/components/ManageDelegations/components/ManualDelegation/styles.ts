import styled from 'styled-components';

export const ManageDelegationsContainer = styled.div`
  .delegation-buttons {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;
