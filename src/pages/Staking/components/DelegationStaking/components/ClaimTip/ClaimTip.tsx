import { useTranslation } from 'react-i18next';

import Tip from 'ui/Tip';

import { useQVault } from 'store/q-vault/hooks';

function ClaimTip () {
  const { t } = useTranslation();
  const { delegationStakeInfo } = useQVault();

  if (Number(delegationStakeInfo.totalStakeReward) > 0) {
    return (
      <Tip
        compact
        type="warning"
        style={{ margin: '15px 0 0 0' }}
      >
        {t('DO_NOT_FORGET_CLAIM')}
      </Tip>
    );
  }
  return null;
}

export default ClaimTip;
