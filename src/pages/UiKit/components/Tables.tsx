import ValidatorsTable from 'pages/Staking/components/ValidatorStaking/components/ValidatorsTable';

function Tables () {
  return (
    <div>
      <h2 className="text-h2 block">Table: </h2>
      <div className="block-content">
        <ValidatorsTable />
      </div>
    </div>
  );
}

export default Tables;
