
import { motion } from 'framer-motion';
import styled from 'styled-components';
import { media } from 'styles/media';

import { getModalColor } from './colors';

export const ModalContainer = styled(motion.div)<{ $width: number }>`
  position: fixed;
  z-index: 10000;

  .modal-overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: ${({ theme }) => getModalColor(theme, 'overlay')};
    pointer-events: all;
  }

  .modal-dialog {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: max-content;
    padding: 32px;
    pointer-events: all;
    width: ${({ $width }) => `${$width}px`};

    ${media.lessThan('medium')} {
      width: 100%;
    }
  }

  .modal-close {
    position: absolute;
    top: 12px;
    right: 12px;
  }

  .modal-tip {
    margin-top: 4px;
    color: ${({ theme }) => theme.colors.textSecondary};
  }

  .modal-content {
    margin: 0 -24px;
    padding: 0 24px;
    border: none;
    margin-top: 24px;
    background-color: transparent;
    max-height: 75vh;
    overflow-y: auto;
    overflow-x: overlay;
  }
`;
