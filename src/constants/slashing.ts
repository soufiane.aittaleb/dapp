export enum ObjectionStatus {
  NONE = '0',
  OPEN = '1',
  ACCEPTED = '2',
  PENDING = '3',
  DECIDED = '4',
  EXECUTED = '5',
};
